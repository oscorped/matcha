import axios from 'axios';
import Auth from '../security/Auth';

import { PROFILE_IS_SET_RESULT } from './innerActions';

export const PROFILE_FIRST_SET_REQUEST 	= 'PROFILE_FIRST_SET_REQUEST';
export const PROFILE_FIRST_SET_RESULT 	= 'PROFILE_FIRST_SET_RESULT';
export const PROFILE_FIRST_SET_ERROR 	= 'PROFILE_FIRST_SET_ERROR';
export const SET_SEX_PREFERENCE 		= 'SET_SEX_PREFERENCE';
export const SET_GENDER 				= 'SET_GENDER';
export const GENDER_ERROR 				= 'GENDER_ERROR';
export const SET_ABOUT 					= 'SET_ABOUT';
export const ABOUT_ERROR 				= 'ABOUT_ERROR';
export const SET_BIRTHDATE 				= 'SET_BIRTHDATE';
export const SET_DEFAULT_DATES 			= 'SET_DEFAULT_DATES';

export const setSexPreference = (preference) => {
	return {
		type: SET_SEX_PREFERENCE,
		payload: preference
	}
}

export const setGender = (gender) => {
	return {
		type: SET_GENDER,
		payload: gender
	}
}

export const setErrorGender = () => {
	return {
		type: GENDER_ERROR,
		payload: 'This field is required!'
	};
}

export const setAbout = (about) => {
	return {
		type: SET_ABOUT,
		payload: about
	}
}

export const setErrorAbout = () => {
	return {
		type: ABOUT_ERROR,
		payload: 'This field must be 10 characters least!'
	};
}

export const setBirthdate = (birthdate) => {
	return {
		type: SET_BIRTHDATE,
		payload: birthdate
	}
}

export const setDefaultDates = (bdate, mdate) => {
	return {
		type: SET_DEFAULT_DATES,
		payload: {
			b: bdate,
			m: mdate
		}
	}
}

export const saveFirstStep = (data) => {
	return dispatch => {
		dispatch({
			type: PROFILE_FIRST_SET_REQUEST
		});

		axios.post('http://localhost:3000/api/profile_first_step/' + Auth.getUserId(), {data:data}, {headers: {'Authorization': `bearer ${ Auth.getToken()}`}})
			.then(res => {
				if (res.status !== 200) {
					dispatch({
						type: PROFILE_FIRST_SET_ERROR,
						payload: res.statusText
					});
				} else {
					dispatch({
						type: PROFILE_IS_SET_RESULT,
						payload: res.data
					})
				}
			})
	}
}
