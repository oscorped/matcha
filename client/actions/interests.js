import axios from 'axios';
import Auth from '../security/Auth';

import { PROFILE_IS_SET_RESULT } from './innerActions';

export const LOAD_USER_INTERESTS_REQUEST    = 'LOAD_USER_INTERESTS_REQUEST';
export const LOAD_USER_INTERESTS_RESULT     = 'LOAD_USER_INTERESTS_RESULT';
export const LOAD_USER_INTERESTS_ERROR      = 'LOAD_USER_INTERESTS_ERROR';
export const SAVE_USER_INTERESTS_REQUEST    = 'SAVE_USER_INTERESTS_REQUEST';
export const SAVE_USER_INTERESTS_RESULT     = 'SAVE_USER_INTERESTS_RESULT';
export const SAVE_USER_INTERESTS_ERROR      = 'SAVE_USER_INTERESTS_ERROR';
export const CREATE_NEW_INTEREST            = 'CREATE_NEW_INTEREST';
export const DELETE_USER_INTEREST           = 'DELETE_USER_INTEREST';
export const ADD_NEW_USER_INTEREST          = 'ADD_NEW_USER_INTEREST';

export const loadUserInterests = () => {
    return dispatch => {
        dispatch({
            type: LOAD_USER_INTERESTS_REQUEST
        });

        axios.get('http://localhost:3000/api/tags', {headers: {'Authorization': `bearer ${ Auth.getToken()}`}})
            .then(res => {
                if (res.status !== 200) {
                    dispatch({
                        type: LOAD_USER_INTERESTS_ERROR,
                        payload: res.statusText
                    });
                } else {
                    dispatch({
                        type: LOAD_USER_INTERESTS_RESULT,
                        payload: res.data
                    });
                }
            })
    }
}

export const createNewInterest = (interestId) => {
    return {
        type: CREATE_NEW_INTEREST
    }
}

export const deleteUserInterest = (interest) => {
    return {
        type: DELETE_USER_INTEREST,
        payload: interest
    }
}

export const addNewUserInterest = (interest) => {
    return {
        type: ADD_NEW_USER_INTEREST,
        payload: interest
    }
}

export const saveUserInterests = (interests) => {
    return dispatch => {
        dispatch({
            type: SAVE_USER_INTERESTS_REQUEST
        });

        axios.post('http://localhost:3000/api/profile_second_step/' + Auth.getUserId(), {data: interests}, {headers: {'Authorization': `bearer ${ Auth.getToken()}`}})
            .then(res => {
                if (res.status !== 200) {
                    dispatch({
                        type: SAVE_USER_INTERESTS_ERROR,
                        payload: res.statusText
                    });
                } else {
                    dispatch({
                        type: PROFILE_IS_SET_RESULT,
                        payload: res.data
                    })
                }
            })
    }
}
