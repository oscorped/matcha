import axios from 'axios';
import Auth from '../security/Auth';

import { PROFILE_IS_SET_RESULT } from './innerActions';

export const IMAGE_LOADED			= 'IMAGE_LOADED';
export const DELETE_IMAGE 			= 'DELETE_IMAGE';
export const SUBMIT_IMAGES_REQEST 	= 'SUBMIT_IMAGES_REQEST';
export const SUBMIT_IMAGES_RESULT 	= 'SUBMIT_IMAGES_RESULT';
export const SUBMIT_IMAGES_ERROR 	= 'SUBMIT_IMAGES_ERROR';
export const DELETE_IMAGES_REQEST 	= 'DELETE_IMAGES_REQEST';
export const DELETE_IMAGES_RESULT 	= 'DELETE_IMAGES_RESULT';
export const DELETE_IMAGES_ERROR 	= 'DELETE_IMAGES_ERROR';

export const imageLoaded = (imageName) => {
	return {
		type: IMAGE_LOADED,
		payload: imageName
	}
}

export const imageDelete = (imageName) => {
	return dispatch => {
		dispatch({
			type: DELETE_IMAGES_REQEST,
			payload: imageName
		});

		axios.get('http://localhost:3000/api/delete_image/' + imageName, {headers: {'Authorization': `bearer ${ Auth.getToken()}`}})
			.then(res => {
				if (res.status !== 200) {
					dispatch({
						type: DELETE_IMAGES_ERROR,
						payload: res.statusText
					});
				} else {
					dispatch({
						type: DELETE_IMAGES_RESULT
					});
				}
			})
	}
}

export const imageSubmit = (images) => {
	return dispatch => {
		dispatch({
			type: SUBMIT_IMAGES_REQEST
		});

		axios.post('http://localhost:3000/api/save_images/' + Auth.getUserId(), {data: images}, {headers: {'Authorization': `bearer ${ Auth.getToken()}`}})
			.then(res => {
				if (res.status !== 200) {
					dispatch({
						type: SUBMIT_IMAGES_ERROR,
						payload: res.statusText
					});
				} else {
					dispatch({
						type: PROFILE_IS_SET_RESULT,
						payload: res.data
					})
				}
			})
	}
}
