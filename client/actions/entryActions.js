import axios from 'axios';
import Auth from '../security/Auth';

export const LOGIN_SUBMIT_REQUEST           = 'LOGIN_SUBMIT_REQUEST';
export const REG_SUBMIT_REQUEST             = 'REG_SUBMIT_REQUEST';
export const LOGIN_SUBMIT_SUCCESS           = 'LOGIN_SUBMIT_SUCCESS';
export const REG_SUBMIT_RESULT              = 'REG_SUBMIT_RESULT';
export const LOGIN_SUBMIT_ERROR             = 'LOGIN_SUBMIT_ERROR';

export const SET_LOGIN_ACTION               = 'SET_LOGIN_ACTION';
export const SET_PASSWORD_ACTION            = 'SET_PASSWORD_ACTION';
export const SET_EMAIL_ACTION               = 'SET_EMAIL_ACTION';
export const SET_FNAME_ACTION               = 'SET_FNAME_ACTION';
export const SET_LNAME_ACTION               = 'SET_LNAME_ACTION';
export const SET_PASSWORD_CHECK_ACTION      = 'SET_PASSWORD_CHECK_ACTION';

export const SET_GEOLOCATION = 'SET_GEOLOCATION';

export const CLOSE_DIALOG_ACTION = 'CLOSE_DIALOG_ACTION';

const regResult = (msg) => {
    return {
        type: REG_SUBMIT_RESULT,
        payload: msg
    }
};

export const regSubmit = (entry) => {
    return dispatch => {
        dispatch({
            type: REG_SUBMIT_REQUEST
        });

        axios.post('http://localhost:3000/registration', {
            data: entry
        }).then(res => {
            console.log(res);
            if (res.data.exists) {
                console.log(res.data);
                dispatch(regResult(`User is currently registered!`));                 
            }
            else {
                dispatch(regResult('Successfully registered! You can now login:)'));
            }
        }).catch(error => {
            console.log(error);
            dispatch(regResult("Something went wrong, try again!"));
        });
    }
};

const logError = (error) => {
    return {
        type: LOGIN_SUBMIT_ERROR,
        payload: error
    }
};

const logSuccess = (success) => {
    return {
        type: LOGIN_SUBMIT_SUCCESS,
        payload: success
    }
};

export const logSubmit = ({ login, password }) => {
    return dispatch => {
        dispatch({
            type: LOGIN_SUBMIT_REQUEST
        });

        axios.post('http://localhost:3000/login', {
            login,
            password
        }).then(res => {
            if (!res.data.success)
                dispatch(logError(res.data.message));
            else {
                Auth.authenticate(res.data.token, res.data.user.id);
                dispatch(logSuccess(res.data.message));
            }
        }).catch(error => {
            console.log(error);
        })
    }
};

export const closeDialog = () => {
    return {
        type: CLOSE_DIALOG_ACTION
    }
};

export const setGeolocation = (geolocation) => {
    return {
        type: SET_GEOLOCATION,
        geolocation
    }
}
