import axios from 'axios';
import Auth from '../security/Auth';

export const PROFILE_IS_SET_REQUEST 	= 'PROFILE_IS_SET_REQUEST';
export const PROFILE_IS_SET_RESULT 		= 'PROFILE_IS_SET_RESULT';
export const CONNECTION_ERROR 			= 'CONNECTION_ERROR';
export const NEXT_STEP 					= 'NEXT_STEP';

export const getProfile = (id) => {
	return dispatch => {
		dispatch({
			type: PROFILE_IS_SET_REQUEST
		});

		axios.get('http://localhost:3000/api/get_profile_set_level/' + id, {headers: {'Authorization': `bearer ${ Auth.getToken()}`}})
			.then(res => {
				if (res.status !== 200) {
					dispatch({
						type: CONNECTION_ERROR,
						payload: res.statusText
					});
				} else {
					dispatch({
						type: PROFILE_IS_SET_RESULT,
						payload: res.data
					});
				}
			});
	}
}

export const nextStep = (previous) => {
	return {
		type: NEXT_STEP,
		payload: previous + 1
	}
}
