import fetch        from 'isomorphic-fetch';
import find         from 'lodash/find';

const GEOCODE_URL   = 'https://maps.googleapis.com/maps/api/geocode/json';
const API_KEY       = 'AIzaSyBiDY0n6uJyrm7Vyp48Dt7zAL9UQqSvoN0';

export async function getCountry(params) {
    const res = await fetch(`${GEOCODE_URL}?latlng=${params.lat},${params.lng}&key=${API_KEY}&language=en`);

    if (res.status >= 400) {
        throw new Error('Bad response from server');
    }

    const data = await res.json();

    if (!data.results.length) {
        return '';
    }

    const components = data.results[0].address_components;
    const country = find(components, obj => {
        return obj.types.includes('country');
    });
    const city = find(components, obj => {
        return obj.types.includes('locality');
    });

    const result = {
        country,
        city
    }

    return result;
}