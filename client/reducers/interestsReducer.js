import remove from 'lodash/remove';

import {
    LOAD_USER_INTERESTS_REQUEST,
    LOAD_USER_INTERESTS_RESULT,
    LOAD_USER_INTERESTS_ERROR,
    CREATE_NEW_INTEREST,
    DELETE_USER_INTEREST,
    ADD_NEW_USER_INTEREST
} from './../actions/interests';

const initialState = {
    interests: [],
    user_interests: [],
    loading: false,
    error: {
        is: false,
        msg: ''
    }
};
const interests = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_USER_INTERESTS_REQUEST:
            state = {
                ...state,
                loading: true,
                error: {
                    is: false,
                    msg: ''
                }
            };
            break;

        case LOAD_USER_INTERESTS_RESULT:
            state = {
                ...state,
                loading: false,
                error: {
                    is: false,
                    msg: ''
                },
                interests: action.payload
            };
            break;

        case LOAD_USER_INTERESTS_ERROR:
            state = {
                ...state,
                loading: false,
                error: {
                    is: true,
                    msg: action.payload
                },
            };
            break;

        case CREATE_NEW_INTEREST:
            state = {
                ...state
            };
            break;

        case DELETE_USER_INTEREST:
            state = {...state};
            state.interests.push(action.payload);
            // const index = state.user_interests.indexOf(action.payload);
            // if (index !== -1)
            //     state.user_interests.splice(index, 1);
            // remove(state.user_interests, el => {
            //     if (el === action.payload)
            // })
            state.user_interests = state.user_interests.filter(el => {
                if (el !== action.payload)
                    return true;
                else
                    return false;
            })
            break;

        case ADD_NEW_USER_INTEREST:
            state = {...state};
            state.user_interests.push(action.payload);
            // console.log(state);
            // const i = state.interests.indexOf(action.payload);
            // if (i !== -1) 
            //     state.interests.splice(i, 1);
            // remove(state.in)
            state.interests = state.interests.filter(el => {
                if (el !== action.payload)
                    return true;
                else
                    return false;
            })
            break;

        default:
            break;
    }

    return (state);
}

export default interests;