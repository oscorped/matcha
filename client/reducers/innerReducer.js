import {
	PROFILE_IS_SET_REQUEST,
	PROFILE_IS_SET_RESULT,
	CONNECTION_ERROR,
	NEXT_STEP
} from '../actions/innerActions';

const initialState = {
	loading: false,
	error: {
		is: false,
		msg: '',
	},
    logged: true,
    profile_set: false,
    current_profile_step: 0,
};

const innerReducer = (state = initialState, action) => {
	switch (action.type) {
		case PROFILE_IS_SET_REQUEST:
			state = {
				...state,
				loading: true,
				error: {
					is: false,
					msg: ''
				}
			};
			break;

		case PROFILE_IS_SET_RESULT:
			state = {
				...state,
				loading: false,
				error: {
					is: false,
					msg: ''
				},
				profile_set: action.payload.profile_set,
				current_profile_step: action.payload.current_profile_step
			};
			break;

		case CONNECTION_ERROR:
			state = {
				...state,
				loading: false,
				error: {
					is: true,
					msg: action.payload
				}
			};
			break;

		case NEXT_STEP:
			if (action.payload === 3) {
				state = {
					...state,
					profile_set: true
				};
			} else {
				state = {
					...state,
					current_profile_step: action.payload
				}
			}

		default:
			break;
	}

	return state;
};

export default innerReducer;
