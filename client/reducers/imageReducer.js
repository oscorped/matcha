import {
	IMAGE_LOADED,
	DELETE_IMAGE,
	SUBMIT_IMAGES_REQEST,
	SUBMIT_IMAGES_RESULT,
	SUBMIT_IMAGES_ERROR,
	DELETE_IMAGES_REQEST,
	DELETE_IMAGES_RESULT,
	DELETE_IMAGES_ERROR
} from '../actions/imageActions';

const initialState = {
	images: []
}

const imageReducer = (state = initialState, action) => {
	switch (action.type) {
		case IMAGE_LOADED:
			state.images.push(action.payload);
			break;

		case DELETE_IMAGES_REQEST:
			const index = state.images.indexOf(action.payload);
            if (index !== -1)
                state.images.splice(index, 1);
            break;

		default:
			break;
	}

	return state;
}

export default imageReducer;
