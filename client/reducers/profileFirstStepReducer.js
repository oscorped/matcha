import {
	PROFILE_FIRST_SET_REQUEST,
	PROFILE_FIRST_SET_RESULT,
	PROFILE_FIRST_SET_ERROR,
	SET_SEX_PREFERENCE,
	SET_GENDER,
	SET_ABOUT,
	SET_BIRTHDATE,
	SET_DEFAULT_DATES,
	ABOUT_ERROR,
	GENDER_ERROR
} from '../actions/profileFirstStepActions';

const initialState = {
	sex_preference: 'Heterosexual',
	gender: null,
	about: '',
	birthdate: new Date(new Date().getFullYear() - 12, 11, 31),
	max_date: new Date(new Date().getFullYear() - 12, 11, 31),
	errors: {
		gender: '',
		about: '',
		internet: ''
	},
	button: false
}

const profileFirstStepReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_SEX_PREFERENCE:
			state = {
				...state,
				sex_preference: action.payload
			};
			break;

		case SET_GENDER:
			state = {
				...state,
				errors: {
					gender: ''
				},
				gender: action.payload
			};
			break;

		case GENDER_ERROR:
			state = {
				...state,
				errors: {
					gender: action.payload
				}
			};
			break;

		case SET_ABOUT:
			state = {
				...state,
				errors: {
					about: ''
				},
				about: action.payload
			};
			break;

		case ABOUT_ERROR:
			state = {
				...state,
				errors: {
					about: action.payload
				}
			};
			break;

		case SET_BIRTHDATE:
			state = {
				...state,
				birthdate: action.payload
			};
			break;

		case SET_DEFAULT_DATES:
			state = {
				...state,
				birthdate: action.payload.b,
				max_date: action.pauload.m
			};
			break;

		case PROFILE_FIRST_SET_REQUEST:
			state = {
				...state,
				loading: true,
				errors: {
					gender: '',
					about: ''
				}
			};
			break;

		case PROFILE_FIRST_SET_RESULT:
			state = {
				...state,
				loading: false,
				errors: {
					gender: '',
					about: '',
					internet: ''
				},
			};
			break;

		case PROFILE_FIRST_SET_ERROR:
			state = {
				...state,
				loading: true,
				errors: {
					gender: '',
					about: '',
					internet: action.payload
				}
			};
			break;

		default:
			break;
	}

	if ((state.errors.gender === '' && state.errors.about === '' && state.errors.internet === '') && state.about.length >= 10 && state.gender !== null) {
		state.button = true;
	}

	return state;
}

export default profileFirstStepReducer;
