import {
    LOGIN_SUBMIT_SUCCESS,
    LOGIN_SUBMIT_REQUEST, 
    LOGIN_SUBMIT_ERROR,
    SET_LOGIN_ACTION,
    SET_PASSWORD_ACTION,
    SET_FNAME_ACTION,
    SET_LNAME_ACTION,
    SET_EMAIL_ACTION,
    SET_PASSWORD_CHECK_ACTION,
    REG_SUBMIT_REQUEST,
    REG_SUBMIT_RESULT,
    CLOSE_DIALOG_ACTION,
    SET_GEOLOCATION
} from '../actions/entryActions';

const initialState = {
    login: '',
    password: '',
    fname: '',
    lname: '',
    email: '',
    passwordCheck: '',
    isFetching: false,
    msg: '',
    open: false,
    geolocation: {},
    isAuth: false
};

const entryReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUBMIT_REQUEST:
            state = {
                ...state,
                isFetching: true
            };
            break;

        case REG_SUBMIT_REQUEST:
            state = {
                ...state,
                isFetching: true
            };
            break;

        case REG_SUBMIT_RESULT:
            state = {
                ...state,
                msg: action.payload,
                isFetching: false,
                open: true
            };
            break;

        case LOGIN_SUBMIT_ERROR:
            state = {
                ...state,
                msg: action.payload,
                isFetching: false,
                open: true
            };
            break;

        case LOGIN_SUBMIT_SUCCESS:
            state = {
                ...state,
                msg: action.payload,
                isFetching: false,
                open: false,
                isAuth: true
            };
            break;

        case CLOSE_DIALOG_ACTION:
            state = {
                ...state,
                msg: '',
                open: false
            };
            break;

        case SET_GEOLOCATION:
            state = {
                ...state,
                geolocation: action.geolocation
            }        
    }

    return (state);
};

export default entryReducer;
