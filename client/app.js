import React      from 'react';
import ReactDOM   from 'react-dom';
import axios from 'axios';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from 'material-ui/styles';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

import App      from './src/App.jsx';
import store    from './store';
import theme    from './theme';
import Auth     from './security/Auth';

import { setGeolocation }   from './actions/entryActions';
import { getCountry }       from './shared/api/Geocode';

axios.defaults.headers.common['Authorization'] = `bearer ${ Auth.getToken()}`;

const geolocation = navigator.geolocation ? navigator.geolocation : {
    getCurrentPosition(success, failure) {
        failure("Your browser doesn't support geolocation.");
    }
};

new Promise(resolve => {
    geolocation.getCurrentPosition(async pos => {
        const { latitude, longitude } = pos.coords;
        const geolocation = {
        	lat: latitude,
        	lng: longitude
        };
        const countryInfo = await getCountry(geolocation);
        geolocation.countryCode = countryInfo.country.short_name;
        geolocation.city = countryInfo.city.long_name;
        store.dispatch(setGeolocation(geolocation));

        resolve();
    }, () => {
    	axios({
		 	method:'get',
		 	url:'http://ipinfo.io',
		 	responseType:'stream'
		}).then(function(response) {
			const {
				city,
				country,
				loc
			} = response.data;
			const coords = loc ? loc.split(',') : ['0', '0'];
			const geolocation = {
				city,
				countryCode: country,
				lat: coords[0],
				lng: coords[1]
			}
        	store.dispatch(setGeolocation(geolocation));
		});

        resolve();
    });
});

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider muiTheme={theme}>
            <HashRouter>
                <App/>
            </HashRouter>
        </MuiThemeProvider>
    </Provider>
    , document.getElementById('rot')
);
