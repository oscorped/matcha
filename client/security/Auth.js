class Auth {
    static authenticate(token, id) {
        localStorage.setItem('token', token);
        localStorage.setItem('id', id);
    };

    static isAuthenticated() {
        return localStorage.getItem('token') !== null;
    };

    static deauthenticate() {
        localStorage.removeItem('token');
        localStorage.removeItem('id');
    };

    static getToken() {
        return localStorage.getItem('token');
    };

    static getUserId() {
        return localStorage.getItem('id');
    };
}

export default Auth;
