import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { cyan700, grey600, pinkA200, pinkA400, pinkA100, fullWhite } from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';

const theme = getMuiTheme({
    fontFamily: 'Roboto, sans-serif',
    borderRadius: 2,
    palette: {
        primary1Color: cyan700,
        primary2Color: cyan700,
        primary3Color: grey600,
        accent1Color: pinkA200,
        accent2Color: pinkA400,
        accent3Color: pinkA100,
        textColor: fullWhite,
        secondaryTextColor: (0, fade)(fullWhite, 0.7),
        alternateTextColor: '#303030',
        canvasColor: '#303030',
        borderColor: (0, fade)(fullWhite, 0.3),
        disabledColor: (0, fade)(fullWhite, 0.3),
        pickerHeaderColor: (0, fade)(fullWhite, 0.12),
        clockCircleColor: (0, fade)(fullWhite, 0.12),
    },
    userAgent: navigator.userAgent
});

export default theme;