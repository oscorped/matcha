import {
    createStore,
    combineReducers,
    applyMiddleware
} from 'redux';
import ReduxThunk       from 'redux-thunk';

import entryReducer            from './reducers/entryReducer';
import interestReducer         from './reducers/interestsReducer';
import innerReducer		       from './reducers/innerReducer';
import profileFirstStepReducer from './reducers/profileFirstStepReducer';
import imageReducer            from './reducers/imageReducer';

const logger = store => next => action => {
    console.log('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    return result;
};

const store = createStore(combineReducers({
    entry: entryReducer,
    interests: interestReducer,
    inner: innerReducer,
    firstStep: profileFirstStepReducer,
    image: imageReducer
}),
applyMiddleware(ReduxThunk, logger));

export default store;
