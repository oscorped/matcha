import React from 'react';
import { Switch, Route } from 'react-router-dom';

import EntryPage    from './components/EntryPage.jsx';
import NotFound     from './components/NotFound.jsx';
import Inner        from './components/Inner.jsx';
import Welcome      from './components/Welcome.jsx';

import './base.css';

class App extends React.Component {
    render() {
        return (
            <Switch>
                <Route path={'/signup'} component={EntryPage}/>
                <Route path={'/'} component={Inner}/>
                <Route component={NotFound}/>
            </Switch>
        );
    }
}

export default App;
