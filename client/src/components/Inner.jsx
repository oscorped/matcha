import React                    from 'react';
import PropTypes                from 'prop-types';
import { connect }              from 'react-redux';
import { withRouter }           from 'react-router-dom';
import { HashRouter, Route }    from 'react-router-dom';
import { routes }               from './routes/routes';

import CircularProgress from 'material-ui/CircularProgress';

import Auth         from '../../security/Auth';
import Menu         from './inner/Menu.jsx';
import ModalBlock   from './../components/inner/ModalBlock/ModalBlock.jsx';

import {
    getProfile
} from '../../actions/innerActions';

const MainInnerStyle = {
    textAlign: 'center',
    margin: 'auto'
};

const ProgressStyle = {
    margin: 'auto',
    marginTop: '10%',
    textAlign: 'center'
};

const Loading = () => {
    return (
        <div style={ProgressStyle}>
            <CircularProgress
                size={100}
                thickness={5}
            />
        </div>
    );
}

class Inner extends React.Component {

    componentWillMount() {
        if (!Auth.isAuthenticated())
            return this.props.history.push('/signup');

        // call dispatch action
        this.props.getProfile(Auth.getUserId());
    }

    render() {
        const {
            loading,
            profile_set,
            error
        } = this.props.inner;

        return (
            <HashRouter>
                <div style={MainInnerStyle}>
                    <Menu/>
                    <div>
                        {routes.map((route, index) => (
                            <Route
                                key={index}
                                path={route.path}
                                exact={route.exact}
                                component={route.component}
                            />
                        ))}
                    </div>
                    {
                        loading ? <Loading/> : !profile_set ? <ModalBlock step={0} /> : ''
                    }
                </div>
            </HashRouter>
        );
    }
}

Inner.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    inner: PropTypes.object,
    getProfile: PropTypes.func,
};

const mapStateToProps = (state) => {
    return {
        inner: state.inner
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getProfile: (id) => {
            dispatch(getProfile(id));
        },
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Inner));
