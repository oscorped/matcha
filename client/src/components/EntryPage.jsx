import React            from 'react';
import PropTypes        from 'prop-types';
import { withRouter }   from 'react-router-dom';
import { connect }      from 'react-redux';
import SwipeableViews   from 'react-swipeable-views';

import { Tab, Tabs }    from 'material-ui';
import TextField        from 'material-ui/TextField';
import Button           from 'material-ui/RaisedButton';
import FlatButton       from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog           from 'material-ui/Dialog';

import Auth from '../../security/Auth';

import {
    setLogin,
    setPassword,
    setEmail,
    setFname,
    setLname,
    setPasswordCheck,
    regSubmit,
    closeDialog,
    logSubmit
} from '../../actions/entryActions';

const REGEX = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const EntryStyle = {
    margin: 'auto',
    marginTop: '7%',
    maxWidth: '35%',
    textAlign: 'center'
};

const BtnStyle = {
    marginTop: 25
};

const ProgressStyle = {
    margin: 'auto',
    marginTop: '10%',
    textAlign: 'center'
};

class EntryPage extends React.Component {
    state = {
        slideIndex: 0,
        loginErr: '',
        passwordErr: '',
        fnameErr: '',
        lnameErr: '',
        emailErr: '',
        passwordCheckErr: '',
        loginErr2: '',
        passwordErr2: '',
        login: '',
        password: '',
        regLogin: '',
        regEmail: '',
        regFirstName: '',
        regLastName: '',
        regPassword: '',
        regPasswordCheck: '',
        isRegPossible: false
    };

    componentWillMount() {
        if (Auth.isAuthenticated())
            this.props.history.push('/');
    }

    componentWillReceiveProps(nextProps) {
        const {
            entry,
            history
        } = nextProps;

        if (entry.isAuth) {
            history.push('/');
        }
    }

    handleSlide = (value) => {
        this.setState({
            slideIndex: value
        });
    };

    handleInputChange = async (field, e) => {
        const value = e && e.target ? e.target.value : '';

        switch (field) {
            case 'login':
                this.setState({ login: value });
                break;
            case 'password':
                this.setState({ password: value });
                break;
            case 'registrationLogin':
                this.setState({ regLogin: value });
                break;
            case 'registrationEmail':
                this.setState({ regEmail: value });
                break;
            case 'registrationFirstName':
                this.setState({ regFirstName: value });
                break;
            case 'registrationLastName':
                this.setState({ regLastName: value });
                break;
            case 'registrationPassword':
                this.setState({ regPassword: value });
                break;
            case 'registrationPasswordCheck':
                this.setState({ regPasswordCheck: value });
                break;
            default:
                return;
        }
    }

    onLogButtonClick = () => {
        const {
            history,
            logSubmit
        } = this.props;

        const {
            login,
            password
        } = this.state;

        if (login !== '') {
            this.setState({loginErr2: ''});
        }

        if (password.length >= 1) {
            this.setState({passwordErr2: ''});
        }

        if (login === '') {
            this.setState({loginErr2: 'This field is required!'});

            return;
        } else if (password.length <= 0) {
            this.setState({passwordErr2: 'This field is required!'});

            return;
        }

        logSubmit({ login, password });
    };

    onRegButtonClick = () => {
        const {
            regSubmit
        } = this.props;

        const {
            regLogin,
            regEmail,
            regFirstName,
            regLastName,
            regPassword,
            regPasswordCheck,
            isRegPossible
        } = this.state;

        if (regLogin !== '') {
            this.setState({
                loginErr: '',
                isRegPossible: true
            });
        }

        if (regEmail !== '') {
            this.setState({
                emailErr: '',
                isRegPossible: true
            });
        }

        if (regFirstName !== '') {
            this.setState({
                fnameErr: '',
                isRegPossible: true
            });
        }

        if (regLastName !== '') {
            this.setState({
                lnameErr: '',
                isRegPossible: true
            });
        }

        if (regPassword.length >= 8) {
            this.setState({
                passwordErr: '',
                isRegPossible: true
            });
        }

        if (regPassword === regPasswordCheck) {
            this.setState({
                passwordCheckErr: '',
                isRegPossible: true
            });
        }

        if (regLogin === '') {
            this.setState({
                loginErr: 'This field is required!',
                isRegPossible: false
            });
        }

        if (regEmail === '') {
            this.setState({
                emailErr: 'This field is required!',
                isRegPossible: false
            });
        }

        if (regEmail !== '' && !REGEX.test(regEmail)) {
            this.setState({
                emailErr: 'Enter valid Email address!',
                isRegPossible: false
            });
        }

        if (regFirstName === '') {
            this.setState({
                fnameErr: 'This field is required!',
                isRegPossible: false
            });
        }

        if (regLastName === '') {
            this.setState({
                lnameErr: 'This field is required!',
                isRegPossible: false
            });
        }

        if (regPassword.length < 8) {
            this.setState({
                passwordErr: 'Min 8 characters!',
                isRegPossible: false
            });
        }

        if (regPassword !== regPasswordCheck) {
            this.setState({
                passwordCheckErr: 'Passwords don`t match!',
                isRegPossible: false
            });
        }

        if (isRegPossible) {
            regSubmit({
                login: regLogin,
                email: regEmail,
                fname: regFirstName,
                lname: regLastName,
                password: regPassword,
                geolocation: this.props.entry.geolocation
            });
        }
    };

    render() {
        const {
            isFetching,
            msg,
            open
        } = this.props.entry;

        const {
            login,
            password
        } = this.state;

        const actions = [
            <FlatButton
                label="OK"
                primary={true}
                keyboardFocused={true}
                onClick={this.props.closeDialog}
            />
        ];

        if (isFetching) {
            return (
                <div style={ProgressStyle}>
                    <CircularProgress
                        size={100}
                        thickness={5}
                    />
                </div>
            );
        }
        else {
            return (
                <div style={EntryStyle}>
                    <Dialog
                        title="Message"
                        modal={false}
                        actions={actions}
                        open={open}
                        onRequestClose={this.props.closeDialog}
                    >
                        {msg}
                    </Dialog>
                    <Tabs
                        onChange={this.handleSlide}
                        value={this.state.slideIndex}
                    >
                        <Tab label={'Login'} value={0}/>
                        <Tab label={'Registration'} value={1}/>
                    </Tabs>
                    <SwipeableViews
                        index={this.state.slideIndex}
                        onChangeIndex={this.handleSlide}
                    >
                        <div>
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'login')}
                                floatingLabelText='Your username'
                                fullWidth
                                errorText={this.state.loginErr2}
                                value={login}
                            />
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'password')}
                                floatingLabelText='Your password'
                                type='password'
                                fullWidth
                                errorText={this.state.passwordErr2}
                                value={password}
                            />
                            <Button
                                label={'Login'}
                                primary
                                fullWidth
                                style={BtnStyle}
                                onClick={this.onLogButtonClick}
                            />
                        </div>
                        <div>
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'registrationLogin')}
                                floatingLabelText='Your username'
                                fullWidth
                                errorText={this.state.loginErr}
                            />
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'registrationEmail')}
                                floatingLabelText='Your email'
                                fullWidth
                                errorText={this.state.emailErr}
                            />
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'registrationFirstName')}
                                floatingLabelText='Your first name'
                                fullWidth
                                errorText={this.state.fnameErr}
                            />
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'registrationLastName')}
                                floatingLabelText='Your last name'
                                fullWidth
                                errorText={this.state.lnameErr}
                            />
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'registrationPassword')}
                                floatingLabelText='Your password'
                                type='password'
                                fullWidth
                                errorText={this.state.passwordErr}
                            />
                            <TextField
                                onChange={this.handleInputChange.bind(this, 'registrationPasswordCheck')}
                                floatingLabelText='Repeat password'
                                type='password'
                                fullWidth
                                errorText={this.state.passwordCheckErr}
                            />
                            <Button
                                onClick={this.onRegButtonClick}
                                label={'Register'}
                                secondary
                                fullWidth
                                style={BtnStyle}
                            />
                        </div>
                    </SwipeableViews>
                </div>
            );
        }
    }
}

EntryPage.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    entry: PropTypes.object,
    regSubmit: PropTypes.func,
    closeDialog: PropTypes.func,
    logSubmit: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        entry: state.entry
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logSubmit: (entry) => {
            dispatch(logSubmit(entry));
        },
        regSubmit: (entry) => {
            dispatch(regSubmit(entry));
        },
        closeDialog: () => {
            dispatch(closeDialog());
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EntryPage));
