import React from 'react';

import Dating   from '../inner/Dating.jsx';
import CloseBy  from '../inner/CloseBy.jsx';
import Likes    from '../inner/Likes.jsx';
import Messages from '../inner/Messages.jsx';
import Visitors from '../inner/Visitors.jsx';
import Profile  from '../inner/Profile.jsx';
import Settings from '../inner/Settings.jsx';
import About    from '../inner/About.jsx';

export const routes = [
    {
        path: '/dating',
        component: () => <Dating/>
    },
    {
        path: '/closeby',
        component: () => <CloseBy/>
    },
    {
        path: '/chat',
        component: () => <Messages/>
    },
    {
        path: '/likes',
        component: () => <Likes/>
    },
    {
        path: '/visitors',
        component: () => <Visitors/>
    },
    {
        path: '/profile',
        component: () => <Profile/>
    },
    {
        path: '/settings',
        component: () => <Settings/>
    },
    {
        path: '/about',
        component: () => <About/>
    },
    {
        path: '/',
        exact: true,
        component: () => <Profile/> // set deafult page | on production will be Dating page
    }
];
