import React            from 'react';
import PropTypes        from 'prop-types';
import { withRouter }   from 'react-router-dom';

import IconMenu             from 'material-ui/IconMenu';
import IconButton           from 'material-ui/IconButton';
import { List, ListItem }   from 'material-ui/List';
import MoreVertIcon         from 'material-ui/svg-icons/navigation/more-vert';
import AccountIcon          from 'material-ui/svg-icons/action/account-box';
import InfoIcon             from 'material-ui/svg-icons/action/info';
import SettingsIcon         from 'material-ui/svg-icons/action/settings';
import LogoutIcon           from 'material-ui/svg-icons/maps/directions-run';

import { siteUrl } from './../../../../etc/config.json';

import Auth from '../../../security/Auth';

class RightSideMenu extends React.Component
{
    static contextTypes = {
        router : PropTypes.object
    }

    state = {
        isIconOpened: false,
        currentPage : ''
    };

    componentWillMount() {
        const { router } = this.context;

        this.setState({
            currentPage: router.route.location.pathname
        });
    }

    handleMenuToggle = () => {
        this.setState({ isIconOpened: !this.state.isIconOpened });
    }

    handlePageChange = (page) => {
        const { currentPage } = this.state;
        const { history } = this.props;

        if (currentPage !== page) {
            this.setState({
                currentPage: page,
                isIconOpened: false
            });

            history.push(page);
        }
    }

    handleSignUpClick = () => {
        const { history } = this.props;

        Auth.deauthenticate();
        history.push('/signup');
    }

    render () {
        const { isIconOpened } = this.state;

        return (
            <IconMenu
                iconButtonElement={
                    <IconButton>
                        <MoreVertIcon/>
                    </IconButton>
                }
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                animated={true}
                open={isIconOpened}
                onRequestChange={this.handleMenuToggle}
            >
                <List>
                    <ListItem
                        primaryText={'Profile'}
                        leftIcon={<AccountIcon/>}
                        onClick={this.handlePageChange.bind(null, '/profile')}
                    />
                    <ListItem
                        primaryText={'Settings'}
                        leftIcon={<SettingsIcon/>}
                        onClick={this.handlePageChange.bind(null, '/settings')}
                    />
                    <ListItem
                        primaryText={'About'}
                        leftIcon={<InfoIcon/>}
                        onClick={this.handlePageChange.bind(null, '/about')}
                    />
                    <ListItem
                        primaryText={'Logout'}
                        leftIcon={<LogoutIcon/>}
                        onClick={this.handleSignUpClick}
                    />
                </List>
            </IconMenu>
        );
    }
}

RightSideMenu.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired
};

export default withRouter(RightSideMenu);
