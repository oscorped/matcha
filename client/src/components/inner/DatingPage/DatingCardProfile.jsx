import React 		from 'react';
import PropTypes 	from 'prop-types';
import {
    GoogleMap,
    withGoogleMap,
    Marker,
    withScriptjs
} from 'react-google-maps';
import {
    CardTitle
} from 'material-ui/Card';
import LinearProgress       from 'material-ui/LinearProgress';

import { googleMapsAPI }    from './../../../../../etc/config.json';

import './DatingCardProfile.css';

const fameRatingStyle = {
    marginBottom: '20px',
    height: '10px'
};

export default class DatingCardProfile extends React.Component
{
	static propTypes = {
        geolocation : PropTypes.object,
        user        : PropTypes.object
    }

    state = {};

    render() {
        const { geolocation, user } = this.props;
        const {
            userInfo,
            fameRating
        } = user;
        const lat = geolocation && geolocation.lat ? Number(geolocation.lat) : 0;
        const lng = geolocation && geolocation.lng ? Number(geolocation.lng) : 0;

        const MapWithAMarker = withScriptjs(withGoogleMap(() =>
            <GoogleMap
                defaultZoom={9}
                defaultCenter={{ lat, lng }}
            >
                <Marker position={{ lat, lng }} />
            </GoogleMap>
        ));

    	return (
    		<div className='DatingCardProfile'>
                <CardTitle
                    title={userInfo}
                    subtitle={'PUT InterestsBlock HERE'}
                    className='DatingCardProfile__title'
                />
                <LinearProgress
                    mode='determinate'
                    value={Number(fameRating)}
                    style={fameRatingStyle}
                    color={'#55ebc4'}
                />
                <MapWithAMarker
                    googleMapURL={googleMapsAPI}
                    loadingElement={<div style={{ height: '300px' }} />}
                    containerElement={<div style={{ height: '300px', width: '100%' }} />}
                    mapElement={<div style={{ height: '300px' }} />}
                />
    		</div>
    	);
    }
}
