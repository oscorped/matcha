import React        from 'react';
import PropTypes    from 'prop-types';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import ArrowDown            from 'material-ui/svg-icons/navigation/arrow-downward';

import './DatingCardHeader.css';

const showProfileButton = {
    position: 'absolute',
    right: '15px',
    top: '50px'
};

export default class DatingCardHeader extends React.Component
{
    static propTypes = {
        user    : PropTypes.object,
        onClick : PropTypes.func
    }

    state = {};

    render() {
        const { user, onClick } = this.props;
        const {
            nickname,
            age,
            commonInterests
        } = user;

        return (
            <div className='DatingCardHeader'>
                <div className='DatingCardHeader__main-info'>
                    <span>{`${nickname}, ${age}`}</span>
                </div>
                <div className='DatingCardHeader__interests'>
                    <span>{`${commonInterests} common interests`}</span>
                </div>
                <FloatingActionButton style={showProfileButton}  backgroundColor={'#fff'} onClick={onClick}>
                    <ArrowDown style={{ fill: '#05dc09', width: '36px' }} />
                </FloatingActionButton>
            </div>
        );
    }
} 