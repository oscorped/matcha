import React 		from 'react';
import PropTypes 	from 'prop-types';

import {
	CardTitle
} from 'material-ui/Card';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Favorite 			from 'material-ui/svg-icons/action/favorite';
import ClearIcon            from 'material-ui/svg-icons/content/clear';

const dislikeButtonStyle = {
	position: 'absolute',
	left: '15px',
	top: '12px'
};

const likeButtonStyle = {
	position: 'absolute',
	right: '15px',
	top: '12px'
};

export default class DatingCardHeader extends React.Component
{
	static propTypes = {
		onLikeClick	: PropTypes.func,
		onDislikeClick	: PropTypes.func
    }

    state = {};

    render() {
    	const {
    		onDislikeClick,
    		onLikeClick
    	} = this.props;

    	return (
    		<CardTitle style={{ height: '50px' }}>
    			<FloatingActionButton
	    			label="Dislike"
	    			style={dislikeButtonStyle}
	    			backgroundColor={'#fff'}
	    			onClick={onDislikeClick}
    			>
    				<ClearIcon style={{ fill: '#fe5369', width: '36px' }} />
    			</FloatingActionButton>
    			<FloatingActionButton
	    			label="Like"
	    			style={likeButtonStyle}
	    			backgroundColor={'#fff'}
	    			onClick={onLikeClick}
    			>
    				<Favorite style={{ fill: '#55eac4', width: '36px' }} />
    			</FloatingActionButton>
    		</CardTitle>
    	);
    }
} 