import React            from 'react';
import PropTypes        from 'prop-types';
import { withRouter }   from 'react-router-dom';
import { connect }      from 'react-redux';
import drop             from 'lodash/drop';

import {
    Card,
    CardActions,
    CardHeader,
    CardMedia,
    CardTitle,
    CardText
} from 'material-ui/Card';

import animateScrollTo          from 'animated-scroll-to';

import DatingCardHeader         from './DatingPage/DatingCardHeader.jsx';
import DatingCardMediaOverlay   from './DatingPage/DatingCardMediaOverlay.jsx';
import DatingCardProfile        from './DatingPage/DatingCardProfile.jsx';
import DatingCardEmpty          from './DatingPage/DatingCardEmpty.jsx';

import { imgPath } from './../../../../etc/config.json';
import './Dating.css';

const users = [
    {
        id: '1',
        nickname: 'Garold The Great',
        age: '45',
        commonInterests: '3',
        img: 'garold.jpg',
        userInfo: 'I`m Garold The Great',
        fameRating: '30'
    },
    {
        id: '2',
        nickname: 'Somebody that i used to know',
        age: '33',
        commonInterests: '4',
        img: 'gotye.jpg',
        userInfo: 'I`m Somebody that i used to know',
        fameRating: '40'
    },
    {
        id: '3',
        nickname: 'John Cena',
        age: '22',
        commonInterests: '2',
        img: 'cena.jpg',
        userInfo: 'I`m John Cena',
        fameRating: '50'
    },
    {
        id: '4',
        nickname: 'Da wae',
        age: '66',
        commonInterests: '6',
        img: 'dawae.jpg',
        userInfo: 'I`m Da wae',
        fameRating: '60'
    },
    {
        id: '5',
        nickname: 'The Rock',
        age: '55',
        commonInterests: '1',
        img: 'rock.jpg',
        userInfo: 'I`m The Rock',
        fameRating: '70'
    },
    {
        id: '6',
        nickname: 'MEME',
        age: '24',
        commonInterests: '0',
        img: '9gag.jpg',
        userInfo: 'I`m MEME',
        fameRating: '80'
    }
];

class Dating extends React.Component
{
    static propTypes = {
        entry: PropTypes.object
    }

    static contextTypes = {
        router : PropTypes.object
    }

    state = {
        expanded: false,
        users: [],
        currentUser: users.length ? users[0] : {},
        isDatingCardEmpty: !users.length || false
    };

    componentWillMount() {
        this.setState({ users });
    }

    handleShowProfileClick = () => {
        const {
            expanded
        } = this.state;

        this.setState({
            expanded: !expanded
        }, this.animatedScroll);
    }

    handleLikeClick(id) {
        this.removeFromArray();
        this.setState({ expanded: false });
    }


    handleDislikeClick(id) {
        this.removeFromArray();
        this.setState({ expanded: false });
    }

    animatedScroll() {
        if (this.state.expanded) {
            animateScrollTo(document.querySelector('.DatingCardProfile'), { speed: 1000 });
        }
    }

    removeFromArray() {
        const { users } = this.state;
        const newArray = drop(users, 1);
        const currentUser = newArray.length ? newArray[0] : {};

        this.setState({
            users: newArray,
            currentUser,
            isDatingCardEmpty: !newArray.length || false
        });
    }

    renderDatingCard() {
        const { entry } = this.props;
        const { geolocation } = entry;
        const { expanded, currentUser } = this.state;

        return (
            <Card
                className={'DatingCard__visible'}
                style={{ marginTop: '50px' }}
                expanded={expanded}
            >
                <CardHeader
                    children={
                        <DatingCardHeader
                            user={currentUser}
                            onClick={this.handleShowProfileClick.bind(this)}
                        />
                    }
                />
                <CardMedia
                    overlay={
                        <DatingCardMediaOverlay
                            onLikeClick={this.handleLikeClick.bind(this, currentUser.id)}
                            onDislikeClick={this.handleDislikeClick.bind(this, currentUser.id)}
                        />
                    }
                >
                    <div
                        className={'DatingCard__profile-img'}
                        style={{ backgroundImage: `url(${imgPath}/${currentUser.img})` }}
                    />
                </CardMedia>
                <CardText
                    expandable={true}
                    children={
                        <DatingCardProfile
                            geolocation={geolocation}
                            user={currentUser}
                        />
                    }
                />
            </Card>
        );
    }

    render() {
        const { isDatingCardEmpty } = this.state;
        const datingCards = !isDatingCardEmpty ? this.renderDatingCard() : <DatingCardEmpty />;

        return (
            <div className='DatingCard'>
                {datingCards}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        entry: state.entry
    }
};

export default withRouter(connect(mapStateToProps)(Dating));
