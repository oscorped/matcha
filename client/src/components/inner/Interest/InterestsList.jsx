import React        from 'react';
import PropTypes    from 'prop-types';

import Chip from 'material-ui/Chip';

import './InterestsList.css';

const interestStyle = {
    margin: '15px 0 10px 5px'
};

export default class InterestsList extends React.Component
{
    static propTypes = {
        interests: PropTypes.array
    }

    state = {
        selectedInterest: this.props.interests[0]
    }

    handleInterestClick = (interest) => {
        this.setState({ selectedInterest: interest });
    }

    render() {
        const {
            interests
        } = this.props;
        const {
            selectedInterest
        } = this.state;

        return (
            <div className='InterestsList'>
                {
                    interests.map((interest, i) => {
                        const isSelected = Boolean(selectedInterest === interest);

                        return(
                            <Chip
                                key={i}
                                onClick={this.handleInterestClick.bind(null, interest)}
                                style={interestStyle}
                                backgroundColor={isSelected ? '#ff4081' : '#0097a7'}
                                labelColor={'#000'}
                            >
                                {interest}
                            </Chip>
                        );
                    })
                }
            </div>
        );
    }
}
