import React            from 'react';
import PropTypes        from 'prop-types';
import { withRouter }   from 'react-router-dom';
import { connect }      from 'react-redux';

import AutoComplete from 'material-ui/AutoComplete';

import {
    createNewInterest,
    deleteUserInterest,
    addNewUserInterest
} from './../../../../actions/interests';

import './SelectInterestBlock.css';

class SelectInterestBlock extends React.Component
{
    static propTypes = {
        interests           : PropTypes.array,
        createNewInterest   : PropTypes.func,
        deleteUserInterest  : PropTypes.func,
        addNewUserInterest  : PropTypes.func
    }

    state = {
        interests: [],
        currentInterest: ''
    }

    componentWillMount() {
        const {
            interests
        } = this.props;

        this.setState({
            interests
        });
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }

    handleInputUpdate(searchText, dataSource, params) {
        this.setState({
            currentInterest: searchText
        });
    }

    handleAddNewInterest(chosenRequest, index) {
        if (index === -1) {
            createNewInterest(chosenRequest);
        }

        addNewUserInterest(chosenRequest);

        this.setState({
            currentInterest: ''
        });
    }

    render() {
        const {
            interests,
            currentInterest
        } = this.state;

        return (
            <div className='SelectInterestBlock'>
                <AutoComplete
                    hintText='Search for interests'
                    filter={AutoComplete.fuzzyFilter}
                    dataSource={interests}
                    maxSearchResults={5}
                    fullWidth={true}
                    onUpdateInput={this.handleInputUpdate.bind(this)}
                    onNewRequest={this.handleAddNewInterest.bind(this)}
                    searchText={currentInterest}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        interests: state.interests.interests
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createNewInterest: () => {
            dispatch(createNewInterest());
        },
        deleteUserInterest: () => {
            dispatch(deleteUserInterest());
        },
        addNewUserInterest: () => {
            dispatch(addNewUserInterest())
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SelectInterestBlock));
