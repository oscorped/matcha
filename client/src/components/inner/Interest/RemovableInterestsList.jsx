import React        from 'react';
import PropTypes    from 'prop-types';
import { connect }  from 'react-redux';

import Chip from 'material-ui/Chip';

import './RemovableInterestsList.css';

import {
    addNewUserInterest
} from '../../../../actions/interests';

const removableInterestsListStyle = {
    margin: '15px 0 10px 5px'
};

class RemovableInterestsList extends React.Component
{
    // static propTypes = {
    //     interests: PropTypes.array
    // }

    handleRequestDelete = (interestId) => {
        console.log(interestId);
    }

    render() {
        const {
            interests
        } = this.props.interests;

        return (
            <div className='RemovableInterestsList'>
                {
                    interests.map((interest, i) => {
                        return(
                            <Chip
                                key={i}
                                style={removableInterestsListStyle}
                                labelColor={'#000'}
                                onClick={() => this.props.addNewUserInterest(interests[i])}
                                onRequestDelete={this.handleRequestDelete.bind(null, i)}
                            >
                                {interest}
                            </Chip>
                        );
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        interests: state.interests
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addNewUserInterest: (interest) => {
            dispatch(addNewUserInterest(interest));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RemovableInterestsList);
