import React        from 'react';
import PropTypes    from 'prop-types';
import { connect }  from 'react-redux';

import Chip from 'material-ui/Chip';

import './RemovableInterestsList.css';

import {
    addNewUserInterest,
    deleteUserInterest
} from '../../../../actions/interests';

const removableInterestsListStyle = {
    margin: '15px 0 10px 5px'
};

class UserInterestsList extends React.Component
{
    // static propTypes = {
    //     interests: PropTypes.array
    // }

    handleRequestDelete = (interestId) => {
        console.log(interestId);
    }

    render() {
        const interests = this.props.interests.user_interests;
        console.log(interests);

        return (
            <div className='RemovableInterestsList'>
                {
                    interests.map((interest, i) => {
                        return(
                            <Chip
                                key={i}
                                style={removableInterestsListStyle}
                                labelColor={'#000'}
                                
                                onRequestDelete={() => this.props.deleteUserInterest(interests[i])}
                            >
                                {interest}
                            </Chip>
                        );
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        interests: state.interests
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addNewUserInterest: (interest) => {
            dispatch(addNewUserInterest(interest));
        },
        deleteUserInterest: (interest) => {
            dispatch(deleteUserInterest(interest));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInterestsList);
