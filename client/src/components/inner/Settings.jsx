import React from 'react';

import {
    Card,
    CardActions,
    CardTitle
} from 'material-ui/Card';
import RaisedButton     from 'material-ui/RaisedButton';
import Slider           from 'material-ui/Slider';
import LinearProgress   from 'material-ui/LinearProgress'

import InterestsList    from './Interest/InterestsList.jsx';

import './Settings/Settings.css';

const headerCardStyle = {
    height          : '65px',
    marginBottom    : '15px'
};

const filterCardStyle = {
    marginBottom: '15px'
};

const sortCardStyle = {};

const sliderStyle = {
    width   : '80%',
    margin  : '0 auto'
};

const fameRatingStyle = {
    width   : '80%',
    height  : '15px',
    margin  : '0 auto'
};

class Settings extends React.Component
{
    state = {
        sexPreference   : 'Heterosexual',
        searchRange     : 1,
        age             : 16,
        fameRating      : 40,
        ageSort         : 'Ascending',
        interests       : [
            'Swimming',
            'Books',
            'Cars',
            'Table games'
        ]
    }

    handleSexPreferenceChange = (pref) => {
        this.setState({ sexPreference: pref });
    }

    hangleSearchRangeChange = (e, range) => {
        this.setState({ searchRange: range });
    }

    handleAgeSortChange = (value) => {
        this.setState({ ageSort: value });
    }

    render() {
        const {
            sexPreference,
            searchRange,
            fameRating,
            ageSort,
            interests
        } = this.state;

        const isHeterosexual = Boolean(sexPreference === 'Heterosexual');
        const isAsexual = Boolean(sexPreference === 'Asexual');
        const isBisexual = Boolean(sexPreference === 'Bisexual');
        const isHomosexual = Boolean(sexPreference === 'Homosexual');

        const isAgeSortAsc = Boolean(ageSort === 'Ascending');
        const isAgeSortDesc = Boolean(ageSort === 'Descending');

        return (
            <div className='Settings'>
                <Card style={headerCardStyle}>
                    <CardTitle
                        title='Settings'
                    />
                </Card>
                <Card style={filterCardStyle}>
                    <CardTitle
                        title='Filter'
                    />
                    <CardActions>
                        <RaisedButton
                            className='Settings__sex-preference'
                            label='Heterosexual'
                            primary={!isHeterosexual}
                            secondary={isHeterosexual}
                            onClick={this.handleSexPreferenceChange.bind(this, 'Heterosexual')}
                        />
                        <RaisedButton
                            className='Settings__sex-preference'
                            label='Asexual'
                            primary={!isAsexual}
                            secondary={isAsexual}
                            onClick={this.handleSexPreferenceChange.bind(this, 'Asexual')}
                        />
                        <RaisedButton
                            className='Settings__sex-preference'
                            label='Bisexual'
                            primary={!isBisexual}
                            secondary={isBisexual}
                            onClick={this.handleSexPreferenceChange.bind(this, 'Bisexual')}
                        />
                        <RaisedButton
                            className='Settings__sex-preference'
                            label='Homosexual'
                            primary={!isHomosexual}
                            secondary={isHomosexual}
                            onClick={this.handleSexPreferenceChange.bind(this, 'Homosexual')}
                        />
                    </CardActions>
                    <CardActions>
                        <Slider
                            min={0}
                            max={50}
                            step={1}
                            value={searchRange}
                            onChange={this.hangleSearchRangeChange}
                            style={sliderStyle}
                            sliderStyle={{ marginBottom: '15px' }}
                        />
                        <div className='Settings__search-range'>
                            <span>{`Your search range: ${searchRange} km`}</span>
                        </div>
                    </CardActions>
                    <CardActions>
                        <div className='Settings__fame-rate'>
                            <LinearProgress
                                mode='determinate'
                                value={fameRating}
                                style={fameRatingStyle}
                                color='#ff4081'
                            />
                        </div>
                        <div className='Settings__fame-rate-text'>
                            <span>{`Your fame rate: ${fameRating}%`}</span>
                        </div>
                    </CardActions>
                </Card>
                <Card style={sortCardStyle}>
                    <CardTitle
                        title='Sort'
                        style={{ paddingBottom: '0' }}
                    />
                    <div className='Settings__sort-settings'>
                        <div className='Settings__sort-age'>
                            <span>Age</span>
                            <div className='Settings__sort-btn'>
                                <RaisedButton
                                    className='Settings__sex-preference'
                                    label='Ascending'
                                    primary={!isAgeSortAsc}
                                    secondary={isAgeSortAsc}
                                    onClick={this.handleAgeSortChange.bind(null, 'Ascending')}
                                />
                                <RaisedButton
                                    className='Settings__sex-preference'
                                    label='Descending'
                                    primary={!isAgeSortDesc}
                                    secondary={isAgeSortDesc}
                                    onClick={this.handleAgeSortChange.bind(null, 'Descending')}
                                />
                            </div>
                        </div>
                        <div className='Settings__sort-interests'>
                            <span>Main interest</span>
                            <InterestsList interests={interests} />
                        </div>
                        <div style={{ height: '20px' }} />
                    </div>
                </Card>
            </div>
        );
    }
}

export default Settings;
