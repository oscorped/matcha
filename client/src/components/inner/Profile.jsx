import React from 'react';
import axios from 'axios';

import Auth  from '../../../security/Auth';

import InterestList     from './Interest/InterestsList.jsx';
import ProfileImages    from './Profile/ProfileImages.jsx';

import './Profile/Profile.css';

class Profile extends React.Component
{
	state = {
		interests: []
	}

	componentWillMount() {
		// axios.get('http://localhost:3000/api/tags')
  //           .then(res => {
  //               if (res.status !== 200) {
  //                   console.log(res);
  //               } else {
  //                   this.setState({interests: res.data});
  //               }
  //           });

        axios.get('http://localhost:3000/api/get_profile/' + Auth.getUserId())
            .then(res => {
                if (res.status !== 200) {
                    console.log(res);
                } else {
                    console.log(res.data);
                }
            })
	}

    render() {
        return (
        	<div className='Profile'>
                <ProfileImages />
            	<InterestList interests={this.state.interests}/>
            </div>
        );
    }
}

export default Profile;