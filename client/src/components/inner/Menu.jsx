import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import AppBar               from 'material-ui/AppBar';
import Drawer               from 'material-ui/Drawer';
import { List, ListItem }   from 'material-ui/List';
import LikeIcon             from 'material-ui/svg-icons/action/favorite';
import MessageIcon          from 'material-ui/svg-icons/communication/message';
import LocationIcon         from 'material-ui/svg-icons/maps/add-location';
import VisitorIcon          from 'material-ui/svg-icons/social/group';
import DatingIcon           from 'material-ui/svg-icons/action/find-replace';
import ClearIcon            from 'material-ui/svg-icons/content/clear';

import RightSideMenu from './RightSideMenu.jsx';

class Menu extends React.Component
{
    constructor() {
        super();
    }

    static contextTypes = {
        router : PropTypes.object
    }

    state = {
        open        : false,
        currentPage : ''
    };

    componentWillMount() {
        const { router } = this.context;

        this.setState({
            currentPage: router.route.location.pathname
        });
    }

    handleDrawer = () => {
        this.setState({
            open: !this.state.open
        });
    }

    handlePageChange = (page) => {
        const { currentPage } = this.state;
        const { history } = this.props;

        if (currentPage !== page) {
            this.setState({
                currentPage: page,
                open: false
            });

            history.push(page);
        }
    }

    render() {
        const { 
            open
        } = this.state;

        return (
            <div>
                <AppBar
                    title={"Matcha"}
                    iconElementRight={<RightSideMenu/>}
                    onLeftIconButtonTouchTap={this.handleDrawer}
                />
                <Drawer
                    docked={false}
                    width={200}
                    open={open}
                    style={{textAlign: 'left', width: '15%'}}
                >
                    <AppBar
                        title={"Menu"}
                        iconElementLeft={<ClearIcon style={{ fill: '#303030' }}/>}
                        onLeftIconButtonTouchTap={this.handleDrawer}
                        style={{textAlign: 'center'}}
                        iconStyleLeft={{ position: 'absolute', cursor: 'pointer', top: '11px' }}
                    />
                    <List>
                        <ListItem
                            primaryText={"Dating"}
                            leftIcon={<DatingIcon/>}
                            onClick={this.handlePageChange.bind(null, '/dating')}
                        />
                        <ListItem
                            primaryText={"CloseBy"}
                            leftIcon={<LocationIcon/>}
                            onClick={this.handlePageChange.bind(null, '/closeby')}
                        />
                        <ListItem
                            primaryText={"Messages"}
                            leftIcon={<MessageIcon/>}
                            onClick={this.handlePageChange.bind(null, '/chat')}
                        />
                        <ListItem
                            primaryText={"Likes"}
                            leftIcon={<LikeIcon/>}
                            onClick={this.handlePageChange.bind(null, '/likes')}
                        />
                        <ListItem
                            primaryText={"Visitors"}
                            leftIcon={<VisitorIcon/>}
                            onClick={this.handlePageChange.bind(null, '/visitors')}
                        />
                    </List>
                </Drawer>
            </div>
        );
    }
}

Menu.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired
};

export default withRouter(Menu);
