import React        from 'react';
import PropTypes    from 'prop-types';

import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';

import './ModalRegistrationHeader.css';

export default class ModalRegistrationHeader extends React.Component
{
    static propTypes = {
        step: PropTypes.number
    }

    render() {
        const { step } = this.props;

        return (
            <div className='ModalRegistrationHeader'>
                <div className='ModalRegistrationHeader__title'>
                    <span>Primary registration</span>
                </div>
                <Stepper activeStep={step}>
                    <Step>
                        <StepLabel>About</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Interests</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Photo</StepLabel>
                    </Step>
                </Stepper>
            </div>
        );
    }
}
