import React        from 'react';
import PropTypes    from 'prop-types';
import { connect }  from 'react-redux';

import Dialog       from 'material-ui/Dialog';
import SelectField  from 'material-ui/SelectField';
import MenuItem     from 'material-ui/MenuItem';
import TextField    from 'material-ui/TextField';
import DatePicker   from 'material-ui/DatePicker';
import RaisedButton from 'material-ui/RaisedButton';

import ModalRegistrationHeader from './ModalRegistrationHeader.jsx';
import RemovableInterestsList  from './../Interest/RemovableInterestsList.jsx';
import UserInterestsList  from './../Interest/UserInterestsList.jsx';
import SelectInterestBlock     from './../Interest/SelectInterestBlock.jsx';
import ImageLoader             from './../ImageLoader/ImageLoader.jsx';

import {
    setSexPreference,
    setGender,
    setAbout,
    setBirthdate,
    setDefaultDates,
    saveFirstStep,
    setErrorAbout,
    setErrorGender
} from '../../../../actions/profileFirstStepActions';

import {
    loadUserInterests,
    addNewUserInterest,
    saveUserInterests
} from '../../../../actions/interests';

import {
    imageSubmit
} from '../../../../actions/imageActions';

import Auth from '../../../../security/Auth';

import './ModalBlock.css';

class ModalBlock extends React.Component
{

    state = {
        open: true,
    }

    componentWillMount() {
        if (Auth.isAuthenticated())
            this.props.loadUserInterests();
    }

    handleStep = () => {
        const step = this.props.inner.current_profile_step;

        if (step == 0) {
            if (this.props.firstStep.about.length < 10)
                return this.props.setErrorAbout();
            if (!this.props.firstStep.gender)
                return this.props.setErrorGender();
            this.props.saveFirstStep(this.props.firstStep);
        }
        if (step == 1) {
            if (this.props.interests.user_interests.length > 0) {
                this.props.saveUserInterests(this.props.interests.user_interests);
            }
        }
        if (step == 2) {
            if (this.props.image.images.length >= 1) {
                this.props.imageSubmit(this.props.image.images);
            }
        }
    }

    renderFirtsStepRegistration() {
        const {
            sex_preference,
            gender,
            about,
            birthdate,
            max_date,
            errors,
            button
        } = this.props.firstStep;

        return (
            <div className='ModalBlock__form'>
                <div className='ModalBlock__select'>
                    <SelectField
                        floatingLabelText='Sex preference'
                        value={sex_preference}
                        onChange={this.props.setSexPreference}
                    >
                        <MenuItem value={'Heterosexual'} primaryText='Heterosexual' />
                        <MenuItem value={'Asexual'} primaryText='Asexual' />
                        <MenuItem value={'Bisexual'} primaryText='Bisexual' />
                        <MenuItem value={'Homosexual'} primaryText='Homosexual' />
                    </SelectField>
                    <SelectField
                        floatingLabelText='Gender'
                        value={gender}
                        onChange={this.props.setGender}
                        floatingLabelFixed={true}
                        hintText={'Gender'}
                        errorText={errors.gender}
                    >
                        <MenuItem value={'Male'} primaryText='Male' />
                        <MenuItem value={'Female'} primaryText='Female' />
                    </SelectField>
                </div>
                <div className='ModalBlock__about'>
                    <TextField
                      hintText='About myself'
                      floatingLabelText='About myself'
                      multiLine={true}
                      rows={1}
                      fullWidth={true}
                      errorText={errors.about}
                      onChange={this.props.setAbout}
                      value={about}
                    />
                </div>
                <div className='ModalBlock__birth'>
                    <DatePicker
                        hintText='Birth date'
                        container='inline'
                        textFieldStyle={{ width: '100%' }}
                        onChange={this.props.setBirthdate}
                        defaultDate={birthdate}
                        autoOk={true}
                        maxDate={max_date}
                    />
                </div>
            </div>
        );
    }

    renderSecondStepRegistration() {
        return (
            <div className='ModalBlock__form'>
                <div className='ModalBlock__interest-list'>
                    <div className='ModalBlock__title'>
                        Select from available tag-interests:                        
                    </div>
                    <RemovableInterestsList interests={this.props.interests.interests} add={addNewUserInterest}/>
                </div>
                <div className='ModalBlock__interest-list'>
                    <div className='ModalBlock__title'>
                        Your tag-interests:                        
                    </div>
                    <UserInterestsList interests={this.props.interests.user_interests} />
                </div>
            </div>
        );
    }

    renderThirdStepRegistration() {
        return (
            <ImageLoader />
        );
    }

    renderCurrentRegistrationStep() {
        const step = this.props.inner.current_profile_step;

        switch (step) {
            case 0:
                return this.renderFirtsStepRegistration();
                break;
            case 1:
                return this.renderSecondStepRegistration();
                break;
            case 2:
                return this.renderThirdStepRegistration();
                break;
            default:
                return this.renderFirtsStepRegistration();
        }
    }

    render() {
        const {
            open,
        } = this.state;

        const step = this.props.inner.current_profile_step;

        return (
            <Dialog
                title={<ModalRegistrationHeader step={step} />}
                open={open}
                contentClassName='ModalBlock'
            >
                {
                    this.renderCurrentRegistrationStep()
                }
                <div className='ModalBlock__submit'>
                    <RaisedButton
                        label='Back'
                        disabled={step === 0}
                        onClick={this.handlePrevStep}
                        style={{ marginRight: 12 }}
                        default={true}
                    />
                    <RaisedButton
                        label={step === 2 ? 'Finish' : 'Next'}
                        primary={true}
                        onClick={this.handleStep}
                    />
                </div>
            </Dialog>
        );
    }
}

ModalBlock.propTypes = {
    step: PropTypes.number,
    interests: PropTypes.object,
    firstStep: PropTypes.object,
    setSexPreference: PropTypes.func,
    setGender: PropTypes.func,
    setAbout: PropTypes.func,
    setBirthdate: PropTypes.func,
    setDefaultDates: PropTypes.func,
}

const mapStateToProps = (state) => {
    return {
        interests: state.interests,
        firstStep: state.firstStep,
        inner: state.inner,
        image: state.image
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setSexPreference: (event, index, preference) => {
            dispatch(setSexPreference(preference));
        },
        setGender: (event, index, gender) => {
            dispatch(setGender(gender));
        },
        setAbout: (e, about) => {
            dispatch(setAbout(about));
        },
        setBirthdate: (e, birthdate) => {
            dispatch(setBirthdate(birthdate));
        },
        setDefaultDates: (bdate, mdate) => {
            dispatch(setDefaultDates(bdate, mdate));
        },
        saveFirstStep: (data) => {
            dispatch(saveFirstStep(data));
        },
        setErrorGender: () => {
            dispatch(setErrorGender());
        },
        setErrorAbout: () => {
            dispatch(setErrorAbout());
        },
        loadUserInterests: () => {
            dispatch(loadUserInterests());
        },
        addNewUserInterest: (interest) => {
            dispatch(addNewUserInterest(interest));
        },
        saveUserInterests: (interests) => {
            dispatch(saveUserInterests(interests));
        },
        imageSubmit: (images) => {
            dispatch(imageSubmit(images));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalBlock);
