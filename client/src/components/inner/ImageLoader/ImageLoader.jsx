import React            from 'react';
import { connect }      from 'react-redux';
import ImagesUploader   from 'react-images-uploader';

import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';

import Auth from '../../../../security/Auth';

import {
    imageLoaded,
    imageDelete
} from '../../../../actions/imageActions';

import './ImageLoader.css';

const imageUploaderClassObject = {
    pseudobuttonContent : 'ImageUploader__pseudoBtn',
    label               : 'ImageUploader__label'
}

class ImageUploader extends React.Component {
    render() {
        return (
            <ImagesUploader
                url='http://localhost:3000/multiple'
                optimisticPreviews
                onLoadEnd={(err, json) => {
                    if (err) {
                        console.error(err);
                    }
                    let parts = json[0].split('/');
                    this.props.imageLoaded(parts[parts.length - 1]);
                }}
                deleteImage = {key => {
                    this.props.imageDelete(this.props.image.images[key]);
                }}
                label='Upload a picture'
                max={5}
                classNames={imageUploaderClassObject}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        image: state.image
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        imageLoaded: (imageName) => {
            dispatch(imageLoaded(imageName))
        },
        imageDelete: (imageName) => {
            dispatch(imageDelete(imageName))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImageUploader);
