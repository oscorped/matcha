import React 	from 'react';
import axios 	from 'axios';
import Slider	from 'react-slick';

import ImageLoader from './../ImageLoader/ImageLoader.jsx';
import { imgPath } from './../../../../../etc/config.json';

import './Profile.css';

export default class ProfileImages extends React.Component
{
	state = {
		images: [
			'garold.jpg',
			'gotye.jpg',
			'cena.jpg',
			'dawae.jpg',
			'rock.jpg'
		]
	}

    render() {
    	const { images } = this.state;
    	const sliderSettings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 968,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        };

        return (
        	<div className='ProfileImages'>
        		<Slider className='ProfileImages__slider' {...sliderSettings}>
                    {
                        images.map( (image, key) => {
                            return (
                                <div
                                    className='ProfileImages__image-container'
                                    data-index={key}
                                    key={key}
                                >
                                	<div className='ProfileImages__image' style={{ backgroundImage: `url(${imgPath}/${image})` }} />
                                </div>
                            );
                        })
                    }
                </Slider>
        		<ImageLoader />
            </div>
        );
    }
}
