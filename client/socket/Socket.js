import openSocket from 'socket.io-client';

const  socket = openSocket('http://localhost:3000');

export const test = (data, callback) => {
	socket.on('outgoing', data => callback(null, data));
	socket.emit('incoming', data);
}