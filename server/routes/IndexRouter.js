import express      from 'express';
import passport     from 'passport';
import * as config  from '../../etc/config.json';

import { asyncMiddleware } from './asyncMiddleware';
 
const assetUrl = config.assetUrl;

import * as conn from '../db/ModelService/ConnectionService';
import * as user from '../db/ModelService/UserService';
import UserModel from '../db/Model/UserModel';

const IndexRouter = express.Router();

IndexRouter.get('/', (req, res) => {
  res.send(`
      <html>
      <head>
          <meta charset="utf-8"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Matcha</title>
          <link rel="stylesheet" href="${assetUrl}/build/bundle.css">
          <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
          <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
      </head>
      <body>
        <div id="rot"></div>
        <script type="application/javascript" src="${assetUrl}/build/build.js"></script>
      </body>
    </html>
  `);
});

IndexRouter.post('/login', (req, res, next) => {
    passport.authenticate('login', (err, token, userData) => {
        if (err) {
            if (err.name === 'IncorrectCredentialsError') {
                return res.json({
                    success: false,
                    message: err.message
                });
            }

            return res.json({
                success: false,
                message: 'Something went wrong!'
            });
        }
        // sleep.sleep(1);
        return res.json({
            success: true,
            message: 'You have successfully logged in!',
            token,
            user: userData
        });
    })(req, res, next);
});

IndexRouter.post('/registration', asyncMiddleware(async (req, res) => {
    if ( await user.exists(req.body.data.email) ) {
      res.json({ exists: 'true' });
    } else {
      const newUser = await user.regUser(req.body.data);
      const newConn = await conn.newUserConnection(newUser._id);
      res.json(newUser);
    }
}));

export default IndexRouter;