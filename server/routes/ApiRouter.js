import express      from 'express';
import fs           from 'fs';

import {
    getAll,
    getUserById,
    saveProfileFirstStep,
    saveProfileSecondStep,
    saveProfileThirdStep,
    getDatingList
} from '../db/ModelService/UserService';

import {
    getTagsAsync,
    createTagAsync
} from '../db/ModelService/TagService';

import { asyncMiddleware } from './asyncMiddleware';

const ApiRouter = express.Router();





// Profile managment

ApiRouter.get('/get_profile/:id', asyncMiddleware(async (req, res) => {
    const user = await getUserById(req.params.id);
    res.json(user);
}))

ApiRouter.get('/get_profile_set_level/:id', asyncMiddleware(async (req, res) => {
    const user = await getUserById(req.params.id);
    let profile_set = false;
    let current_profile_step = 0;
    if (user.profile_set_up_1) {
        current_profile_step = 1;
    }
    if (user.profile_set_up_2) {
        current_profile_step = 2;
    }
    if (user.profile_set_up_3) {
        profile_set = true;
    }

    res.json({profile_set: profile_set, current_profile_step: current_profile_step});
}))

ApiRouter.post('/profile_first_step/:id', asyncMiddleware(async (req, res) => {
	const user = await saveProfileFirstStep(req.params.id, req.body.data);
    res.json({profile_set: false, current_profile_step: 1});
}))

ApiRouter.post('/profile_second_step/:id', asyncMiddleware(async (req, res) => {
	const result = await saveProfileSecondStep(req.params.id, req.body.data);
    res.json({profile_set: false, current_profile_step: 2});
}))

ApiRouter.get('/delete_image/:image', asyncMiddleware(async (req, res) => {
    const image = req.params.image;
    fs.unlinkSync('./server/static/users/' + image);
    res.status(200).end();
}))

ApiRouter.post('/save_images/:id', asyncMiddleware(async (req, res) => {
    const result = await saveProfileThirdStep(req.params.id, req.body.data);
    res.json({profile_set: true, current_profile_step: 3});
}))



// Dating

const getPreferedGender = (gender, preference) => {
    switch (preference) {
        case 'Heterosexual':
            if (gender === 'Male') {
                return ['Female'];
            } else {
                return ['Male'];
            }
            break;

        case 'Homosexual':
            if (gender == 'Male') {
                return ['Male'];
            } else {
                return ['Female'];
            }
            break;

        case 'Bisexual':
            return ['Male', 'Female'];
            break;

        case 'Asexual':
            return ['Male', 'Female'];
            break;
    }
}

ApiRouter.get('/dating_list/:id', asyncMiddleware(async (req, res) => {
    const user = await getUserById(req.params.id);
    // const gender = getPreferedGender(user.gender, user.preference);
    const filter = {
        // _id: { $ne: user._id },
        gender: { $in: getPreferedGender(user.gender, user.preference) },
        age: { $gte: user.search_age.age_min, $lte: user.search_age.age_max }
    };

    const list = await getDatingList(filter);
    res.json(list);
}))



// Tag managment

ApiRouter.get('/tags', asyncMiddleware(async (req, res) => {
    const tag_array = await getTagsAsync();
    const text_array = tag_array.map(el => {
        return el.text;
    })
    res.json(text_array);
}))


ApiRouter.post('/tags', asyncMiddleware(async (req, res) => {
    const result = await createTagAsync(req.body.data.str);
    res.json(result);
}))

export default ApiRouter;