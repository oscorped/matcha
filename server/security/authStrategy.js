import jwt from 'jsonwebtoken';
import Strategy from 'passport-local';
import UserModel from '../db/Model/UserModel';
import config from '../../etc/config.json';

export default new Strategy({
    usernameField: 'login',
    passwordField: 'password',
    session: false,
    passReqToCallback: true
}, (res, login, password, done) => {
    const userData = {
        login: login.trim(),
        password: password.trim()
    };

    return UserModel.findOne({ login: userData.login}, (err, user) => {
        if (err)
            return done(err);

        if (!user) {
            const error = new Error('You are not registered!');
            error.name = 'IncorrectCredentialsError';

            return done(error);
        }

        return user.comparePassword(userData.password, (err, isMatch) => {
            if (err)
                return done(err);

            if (!isMatch)
            {
                const error = new Error('Incorrect password!');
                error.name = 'IncorrectCredentialsError';

                return done(error);
            }

            const payload = {
                sub: user._id
            };

            const token = jwt.sign(payload, config.security.jwtSecret);
            const data = {
                login: user.login,
                id: user._id
            };

            return done(null, token, data);
        });
    });
});
