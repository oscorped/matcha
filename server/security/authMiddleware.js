import jwt from 'jsonwebtoken';
import UserModel from '../db/Model/UserModel';
import config from '../../etc/config.json';

const authMiddleware = (req, res, next) => {
    if (!req.headers.authorization)
        return res.status(401).send();

    const token = req.headers.authorization.split(' ')[1];

    return jwt.verify(token, config.security.jwtSecret, (err, decoded) => {
        if (err)
            return res.status(401).send();

        const userId = decoded.sub;

        return UserModel.findById(userId, (err, user) => {
            if (err || !user)
                res.status(401).send();

            return next();
        });
    });
};

export default authMiddleware;
