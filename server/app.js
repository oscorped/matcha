import express from 'express';
import ioProm from 'express-socket.io';
import * as config from '../etc/config.json';
import bodyParser from 'body-parser';

import corsPrefetch from 'cors-prefetch-middleware';
import imagesUpload from 'images-upload-middleware';

import IndexRouter from './routes/IndexRouter';
import ApiRouter from './routes/ApiRouter';
import authMiddleware from './security/authMiddleware';
import setUpConnection from './db/connect';
import authStrategy from './security/authStrategy';
import socketHandler from './socket/socketHandler';
import passport from 'passport';

const app = express();

const PORT = process.env.PORT || config.port;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

passport.use('login', authStrategy);

setUpConnection();

app.use('/static', express.static('./server/static'));
 
app.use(corsPrefetch);
 
app.post('/multiple', imagesUpload(
    './server/static/users',
    'http://localhost:3000/static/users',
    true
));

app.use('/', IndexRouter);
app.use('/api', authMiddleware);
app.use('/api', ApiRouter);

const server = ioProm.init(app);

ioProm.then(socketHandler);

server.listen(PORT, () => {
    console.log(`Server listening on: ${PORT}`);
});
