import mongoose from 'mongoose';

const setUpConnection = () => {
    mongoose.connect('mongodb://localhost/matcha', {useMongoClient: true});
    mongoose.Promise = global.Promise;
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => {
        console.log('Ready to work');
    });
};

export default setUpConnection;
