import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const Schema = mongoose.Schema;

const UserSchema = Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String
    },
    salt: {
        type: String
    },
    fname: {
        type: String,
    },
    lname: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    search_age: {
        age_min: {
            type: Number,
            default: 12
        },
        age_max: {
            type: Number,
            default: 28
        }
    },
    geo: {
        lat: { type: Number },
        lng: { type: Number },
        countryCode: { type: String },
        city: { type: String },
    },
    main_image: String,
    images: [ String ],
    profile_set_up_1: {
        type: Boolean,
        default: false
    },
    profile_set_up_2: {
        type: Boolean,
        default: false
    },
    profile_set_up_3: {
        type: Boolean,
        default: false
    },
    preference: {
        type: String
    },
    gender: {
        type: String
    },
    about: {
        type: String
    },
    birthdate: {
        type: Date
    },
    age: {
        type: Number
    },
    tags: [ String ]
});

UserSchema.methods.comparePassword = function comparePassword(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

UserSchema.pre('save', function saveHook(next) {
    const user = this;

    if (!user.isModified('password'))
        return next();

    return bcrypt.genSalt(10, (saltError, salt) => {
        if (saltError)
            return next(saltError);

        //user.salt = salt;
        return bcrypt.hash(user.password, salt, (hashError, hash) => {
            if (hashError)
                return next(hashError);

            user.password = hash;
            return next();
        })
    })
});

export default mongoose.model('user', UserSchema);
