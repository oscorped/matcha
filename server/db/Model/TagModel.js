import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const TagSchema = Schema({
	text: String
});

export default mongoose.model('tag', TagSchema);