import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const connectionSchema = new Schema({
	userId: String,
	liked: [String],
	disliked: [String],
	blocked: [String],
	dual_liked: [String],
});

export default mongoose.model('connection', connectionSchema);
