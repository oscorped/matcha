import UserModel from '../Model/UserModel';

export const regUser = data => {
    const user = new UserModel();

    user.login = data.login.trim(),
    user.email = data.email.trim(),
    user.fname = data.fname.trim(),
    user.lname = data.lname.trim(),
    user.password = data.password.trim(),
    user.geo.lat = data.geolocation.lat,
    user.geo.lng = data.geolocation.lng,
    user.geo.countryCode = data.geolocation.countryCode,
    user.geo.city = data.geolocation.city

    return user.save();
};

export const getAll = () => {
    return UserModel.find({});
};

export const getUserById = (id) => {
    return UserModel.findById(id, (err, user) => {
        if (err) {
            console.log(err);
        } else {
            return user;
        }
    })
}

export const saveProfileFirstStep = (id, data) => {
    return UserModel.findById(id, (err, user) => {
        if (err) {
            console.log(err);
        } else {
            user.preference = data.sex_preference;
            user.about = data.about;
            user.gender = data.gender;
            user.birthdate = data.birthdate;
            const temp = new Date();
            const date = new Date(Date.parse(data.birthdate));
            user.age = temp.getYear() - date.getYear();
            user.profile_set_up_1 = true;

            return user.save();
        }
    })
}

export const saveProfileSecondStep = (id, data) => {
    return UserModel.findById(id, (err, user) => {
        if (err) {
            console.log(err);
        } else {
            user.tags = data;
            user.profile_set_up_2 = true;

            return user.save();
        }
    })
}

export const saveProfileThirdStep = (id, images) => {
    return UserModel.findById(id, (err, user) => {
        if (err) {
            console.log(err);
        } else {
            user.images = images.map((el) => {
                return '/server/static/users/' + el;
            });
            user.main_image = user.image[0];
            user.profile_set_up_3 = true;

            return user.save();
        }
    })
}

export const getDatingList = filter => {
    return UserModel.find(filter, (err, list) => {
        if (err) {
            console.log(err);
        } else {
            return list;
        }
    })
}

export const exists = data => {
    const filter = {
        email: data.email
    };
    return UserModel.find(filter, (err, list) => {
        if (err) {
            console.log(err);
        } else {
            if (list.length)
                return true;
            else
                return false;
        }
    })
}
