import TagModel from '../Model/TagModel';

export const getTagsAsync = () => {
	return TagModel.find({}).exec((err, tags) => {
		if (err)
			console.log(err);
		else
			return tags;
	});
}

export const createTagAsync = str => {
	const tag = new TagModel({text: str});

	return tag.save();
}