import ConnectionModel from '../Model/ConnectionModel';

export const newUserConnection = id => {
	const newConn = new ConnectionModel({
		userId: id,
		liked: [],
		disliked: [],
		blocked: [],
		dual_liked: []
	});

	return newConn.save();
}

export const newLikedConnection = (userId, likedId) => {
	return ConnectionModel.findById(userId, (err, conn) => {
		if (err) {
			console.log(err);
		} else {
			conn.liked.push(likedId);

			return conn.save();
		}
	})
}

export const newDislikedConnection = (userId, dislikedId) => {
	return ConnectionModel.findById(userId, (err, conn) => {
		if (err) {
			console.log(err);
		} else {
			conn.disliked.push(dislikedId);

			return conn.save();
		}
	})
}

export const newBlockedConnection = (userId, blockedId) => {
	return ConnectionModel.findById(userId, (err, conn) => {
		if (err) {
			console.log(err);
		} else {
			conn.blocked.push(blockedId);

			return conn.save();
		}
	})
}

export const newDuallikedConnection = (userId, duallikedId) => {
	return ConnectionModel.findById(userId, (err, conn) => {
		if (err) {
			console.log(err);
		} else {
			conn.dual_liked.push(duallikedId);

			return conn.save();
		}
	})
}
